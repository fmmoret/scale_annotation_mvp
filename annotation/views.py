import json
import urllib

from requests import post
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import JsonResponse, HttpResponseBadRequest, HttpResponseNotAllowed, HttpResponse
from bson import ObjectId
from bson.errors import InvalidId
from pymongo import ReturnDocument

from db.mongo_database_utils import getMongoDB
from annotation.validation import create_and_insert_annotation_task, convert_annotation_task_document_to_client_response, validate_and_convert_annotations, InvalidFieldException

db = getMongoDB()

def scaler_backpanel(request):
    context = RequestContext(request)
    return render_to_response('annotation/scaler_backpanel.html', {}, context)

def create_task(request):
    if request.method == 'POST':
        try:
            new_task_document = create_and_insert_annotation_task(
                db,
                request.POST.get('api_key'),
                request.POST.get('attachment'),
                request.POST.get('attachment_type'),
                request.POST.get('callback_url'),
                request.POST.get('instruction'),
                request.POST.getlist('objects_to_annotate'),
                request.POST.get('with_labels'),
            )
        except InvalidFieldException, e:
            return HttpResponseBadRequest(e.message)
        response = convert_annotation_task_document_to_client_response(new_task_document)
        return JsonResponse(response)
    else:
        return HttpResponseNotAllowed('This endpoint accepts POSTs only')


def next_task(request):
    if request.method == 'GET':
        # not useful for more than 1 annotator at a time
        # would need to mark claimed tasks and add
        # expiration for the mark and access to the task for claimed tasks
        next_task_document = db.annotation_tasks.find_one({'status': 'pending'})
        if next_task_document is None:
            return JsonResponse({'message': 'No tasks in the queue'})
        else:
            response = convert_annotation_task_document_to_client_response(next_task_document)
            return JsonResponse(response)
    else:
        return HttpResponseNotAllowed('This endpoint accepts GETs only')

def submit_task(request):
    if request.method == 'POST':
        try:
            parsed_annotations = map(json.loads, request.POST.getlist('annotations[]'))
        except ValueError:
            return HttpResponseBadRequest('Invalid annotations. Could not parse')
        try:
            annotations = validate_and_convert_annotations(parsed_annotations)
        except InvalidFieldException, e:
            return HttpResponseBadRequest(e.message)
        try:
            task_id = ObjectId(request.POST.get('task_id'))
        except InvalidId:
            return HttpResponseBadRequest('Invalid task_id')
        updated_task_document = db.annotation_tasks.find_one_and_update(
            {'_id': task_id, 'status': 'pending'},
            {'$set': {
                'status': 'completed',
                'response': {
                    'annotations': annotations,
                },
            }},
            return_document=ReturnDocument.AFTER
        )
        if updated_task_document is None:
            task_document = db.annotation_tasks.find_one({'_id': task_id}, {'_id':True, 'status':True})
            if task_document is None:
                return HttpResponseBadRequest('Invalid task_id')
            elif task_document['status'] == 'completed':
                return JsonResponse({'message': 'task already completed'})
            else:
                # unexpected
                raise
        # makes sense to make this into its own function instead of
        # re-using the same convert_annotation_task_document_to_client_response function
        # but it works for now
        response = {}
        response['task'] = convert_annotation_task_document_to_client_response(updated_task_document)
        response['task_id'] = str(updated_task_document['_id'])
        response['response'] = updated_task_document['response']
        if not updated_task_document['with_labels']:
            for annotation in response['response']['annotations']:
                del annotation['label']
        try:
            r = post(updated_task_document['callback_url'], json=json.dumps(response))
            # no error handling ^ -- would make sense to schedule this on something else
        except Exception, e:
            import traceback
            print e, traceback.format_exc()

        return JsonResponse({'message': 'success'})
    else:
        return HttpResponseNotAllowed('This endpoint accepts POSTs only')

def test_callback_url(request):
    print 'CALLBACK URL'
    print request.method
    print request.POST
    print request.body
    return HttpResponse('')
