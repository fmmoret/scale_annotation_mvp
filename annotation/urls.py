from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView
from annotation import views

urlpatterns = patterns('',
    url(r'^scaler-backpanel$', views.scaler_backpanel, name='scaler_backpanel'),

    # AJAX:

    # Gets only
    url(r'^next-task$', views.next_task, name='next_task'),
    # Posts only
    url(r'^create-task$', views.create_task, name='create_task'),
    url(r'^submit-task$', views.submit_task, name='submit_task'),
    url(r'^test-callback-url$', views.test_callback_url, name='test_callback_url'),
)

