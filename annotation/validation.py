import httplib2
import pytz

from bson import ObjectId
from bson.errors import InvalidId
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from datetime import datetime
from copy import deepcopy


class InvalidFieldException(Exception):
    pass

def create_and_insert_annotation_task(db, api_key, attachment, attachment_type, callback_url, instruction, objects_to_annotate, with_labels):
    object_id_api_key = validate_and_convert_api_key(db, api_key)
    validate_attachment(attachment)
    validate_attachment_type(attachment_type)
    validate_callback_url(callback_url)
    validate_instruction(instruction)
    boolean_with_labels = validate_and_convert_with_labels(with_labels)
    validate_objects_to_annotate(objects_to_annotate, with_labels)

    new_annotation_task_document = {
        '_id': ObjectId(),
        # the mongodb client does this ^ under the hood before insert
        # just making it explicit for sake of valid task document
        'api_key_used': object_id_api_key,
        'attachment': attachment,
        'attachment_type': attachment_type,
        'callback_url': callback_url,
        'instruction': instruction,
        'objects_to_annotate': objects_to_annotate,
        'status': 'pending',
        # timezone-aware datetime
        'time_created': datetime.now(pytz.utc),
        'with_labels': boolean_with_labels,
    }
    db.annotation_tasks.insert_one(new_annotation_task_document)

    return new_annotation_task_document

def convert_annotation_task_document_to_client_response(annotation_task_document):
    response = {
        'task_id': str(annotation_task_document['_id']),
        'attachment': annotation_task_document['attachment'],
        'attachment_type': annotation_task_document['attachment_type'],
        'callback_url': annotation_task_document['callback_url'],
        'instruction': annotation_task_document['instruction'],
        'objects_to_annotate': annotation_task_document['objects_to_annotate'],
        'status': annotation_task_document['status'],
        'time_created': annotation_task_document['time_created'].isoformat(),
        'with_labels': annotation_task_document['with_labels'],
    }
    return response

def validate_and_convert_api_key(db, api_key):
    if type(api_key) in [str, unicode]:
        try:
            api_key = ObjectId(api_key)
        except InvalidId:
            raise InvalidFieldException('Invalid api key')
    else:
        raise InvalidFieldException('Invalid api key. Must be a string')
    if db.api_keys.find_one({'_id': api_key}) is None:
        raise InvalidFieldException('api key not found')
    else:
        return api_key

def validate_attachment(attachment):
    if type(attachment) not in [str, unicode]:
        raise InvalidFieldException('Invalid attachment url. Must be a string')

    try:
        # does not verify that it exists
        URLValidator()(attachment)
    except ValidationError:
        raise InvalidFieldException('Invalid attachment url')

def validate_attachment_type(attachment_type):
    if attachment_type != 'image':
        raise InvalidFieldException('Only attachment_type \'image\' is currently supported')

def validate_callback_url(callback_url):
    if type(callback_url) not in [str, unicode]:
        raise InvalidFieldException('Invalid callback url. Must be a string')

    try:
        # does not verify that it exists
        URLValidator()(callback_url)
    except ValidationError:
        raise InvalidFieldException('Invalid callback url')

def validate_instruction(instruction):
    if type(instruction) not in [str, unicode]:
        raise InvalidFieldException('Invalid instruction')

def validate_objects_to_annotate(objects_to_annotate, boolean_with_labels):
    if type(objects_to_annotate) == list:
        if boolean_with_labels and len(objects_to_annotate) == 0:
            raise InvalidFieldException('No objects provided in objects_to_annotate')
        if not all(map(lambda label: type(label) in [str, unicode], objects_to_annotate)):
            raise InvalidFieldException('Invalid labels in object_to_annotate. Labels must be strings')
    else:
        raise InvalidFieldException('Invalid objects_to_annotate. Must be a list')

def validate_and_convert_with_labels(with_labels):
    if type(with_labels) in [str, unicode]:
        with_labels = with_labels.strip().lower()
        if with_labels != 'true' and with_labels != 'false':
            raise InvalidFieldException('Invalid with_labels. Must be a boolean, or strings \'true\' or \'false\'')
        return True if with_labels == 'true' else False
    elif type(with_labels) != bool:
        raise InvalidFieldException('Invalid with_labels. Must be a boolean, or strings \'true\' or \'false\'')
    return with_labels

def validate_and_convert_annotations(annotations):
    if type(annotations) == list:
        annotations = deepcopy(annotations)
        valid_annotation_key_set = set(['top', 'left', 'width', 'height', 'label'])
        for annotation in annotations:
            if type(annotation) != dict:
                raise InvalidFieldException('Invalid annotation in annotations. Must be an object')
            if set(annotation.keys()) != valid_annotation_key_set:
                raise InvalidFieldException('Invalid annotation in annotations. Must have keys \'top\', \'left\', \'width\', \'height\', \'label\'')
            for key, value in annotation.items():
                if key == 'label':
                    if type(value) not in [str, unicode]:
                        raise InvalidFieldException('Invalid annotation in annotations. label must be a string')
                else:
                    try:
                        annotation[key] = int(value)
                    except:
                        raise InvalidFieldException('Invalid annotation in annotations. Could not convert one or more of \'top\', \'left\', \'width\', \'height\' to ints')
        return annotations
    else:
        raise InvalidFieldException('Invalid annotations. Must be a list')
