var path = require('path');
const webpack = require('webpack');

const typescriptResolve = {
    alias: {
        lodash$: path.resolve('./node_modules/lodash/lodash.min.js'),
    },
    extensions: ['', '.ts', '.tsx', '.js'],
};

const plugins = [
    new webpack.optimize.CommonsChunkPlugin({
        name: 'commons',
        filename: 'commons-bundle.js',
        minChunks: 2,
    }),
];

const loaders = {
    tsxLoader: {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader!ts-loader?silent=true'
    },
    jsxLoader: {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
    },
    jsImplementationLoader: {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'script-loader'
    },
    tslintLoader: {
        test: /\.tsx?$/,
        loader: 'tslint-loader'
    },
    sourceMapLoader: {
        test: /\.js$/,
        loader: 'source-map-loader'
    },
    jsonLoader: {
        test: /\.json$/,
        loader: 'json-loader'
    },
    scssLoader: {
        test: /\.scss$/,
        exclude: /node_modules/,
        loader: 'style-loader!css-loader!sass-loader',
    },
    cssLoader: {
        test: /\.css$/,
        exclude: /node_modules/,
        loader: 'style-loader!css-loader',
    }
};

module.exports = {
    entry: {
        'scaler-backpanel-app': './typescript/apps/scaler_backpanel/index.tsx',
    },
    output: {
        path: path.resolve('./static/js/bundles'),
        filename: '[name]-bundle.js',
    },
    resolve: typescriptResolve,
    devtool: 'source-map',
    plugins: plugins,
    module: {
        preLoaders: [
            loaders.sourceMapLoader,
            loaders.tslintLoader
        ],
        loaders: [
            loaders.jsonLoader,
            loaders.jsImplementationLoader,
            loaders.jsxLoader,
            loaders.tsxLoader,
            loaders.scssLoader,
            loaders.cssLoader
        ],
        tslint: {
            emitErrors: true,
            failOnHint: true
        }
    }
};
