from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import RedirectView

from main import views

urlpatterns = patterns('',

    # MAIN
    url(r'^$', views.home, name='home'),
    url(r'^home/$', RedirectView.as_view(pattern_name='home')),

    # SUBDIRECTORIES
    url(r'^annotation/', include('annotation.urls', namespace='annotation')),

    url(r'^admin/', include(admin.site.urls)),

)
