from requests import post
r = post('http://localhost:8003/annotation/create-task', {
    'api_key': 'f'*24,
    'attachment': 'https://www.scaleapi.com/client/img/logo-dark.png',
    'attachment_type': 'image',
    'callback_url': 'http://localhost:8004/annotation/test-callback-url',
    'instruction': 'Do that one thing',
    'objects_to_annotate': ['s', 'c', 'a', 'l', 'e', 'scale'],
    'with_labels': True
})
print 'STATUS_CODE', r.status_code
with open('response_page.html', 'wb') as f:
    f.write(r.content)
