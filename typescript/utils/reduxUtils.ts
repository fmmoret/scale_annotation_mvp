import * as _ from 'lodash';

export const updateState = <T, S extends T>(state : S, update : T) : S => {
    return  _.assign(_.cloneDeep(state), update);
};
