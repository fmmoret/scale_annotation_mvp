import * as _ from 'lodash';

import {updateState} from '../../../utils/reduxUtils';
import {ScalerBackpanelAppActionTypes} from '../actions/actionTypes';
import {ToolEnum} from '../constants/ToolEnum';
import {IAnnotation} from '../models/IAnnotation';
import {ICurrentTaskState} from '../models/ICurrentTaskState';
import {IScalerBackpanelAppState} from '../models/IScalerBackpanelAppState';

const scalerBackpanelAppInitialState : IScalerBackpanelAppState = {
    currentAnnotation : null,
    currentLabel : null,
    currentTask : null,
    selectedTool : ToolEnum.ERASER,
    usingTool : false,
};

export const ScalerBackpanelAppReducer = (
    state : IScalerBackpanelAppState = _.cloneDeep(scalerBackpanelAppInitialState),
    // The action types type here lets us automatically narrow the type of the action
    // down to the correct action in that scope. No casting needed.
    action : ScalerBackpanelAppActionTypes.actionTypes,
) : IScalerBackpanelAppState => {
    switch (action.type) {
        case ScalerBackpanelAppActionTypes.selectToolActionType:
            return updateState(state, {
                selectedTool : action.tool,
                currentLabel : action.label,
                currentAnnotation : null as null | IAnnotation,
            });
        case ScalerBackpanelAppActionTypes.startUsingToolActionType:
            return updateState(state, {
                usingTool : true,
            });
        case ScalerBackpanelAppActionTypes.stopUsingToolActionType:
            if (state.currentAnnotation !== null && state.currentTask !== null) {
                const annotations = _.cloneDeep(state.currentTask.annotations);
                annotations.push(state.currentAnnotation);
                const currentTask = updateState(state.currentTask, {annotations});
                return updateState(state, {
                    currentTask : currentTask as ICurrentTaskState | null,
                    currentAnnotation : null as IAnnotation | null,
                    usingTool: false,
                });
            }
            return updateState(state, {
                usingTool : false,
            });
        case ScalerBackpanelAppActionTypes.updateCurrentAnnotationActionType:
            return updateState(state, {
                currentAnnotation: {
                    label : state.currentLabel,
                    top : action.top,
                    left : action.left,
                    width : action.width,
                    height : action.height,
                } as IAnnotation | null,
            });
        case ScalerBackpanelAppActionTypes.eraseAnnotationActionType:
            if (state.currentTask !== null) {
                const annotations = _.cloneDeep(state.currentTask.annotations).filter((value, index) => index !== action.index);
                const currentTask = updateState(state.currentTask, {annotations});
                return updateState(state, {currentTask : currentTask as ICurrentTaskState | null});
            }
            throw new Error('unexpected');
        case ScalerBackpanelAppActionTypes.clearAnnotationsActionType:
            if (state.currentTask !== null) {
                const annotations : IAnnotation[] = [];
                const currentTask = updateState(state.currentTask, {annotations});
                return updateState(state, {currentTask : currentTask as ICurrentTaskState | null});
            }
            throw new Error('unexpected');
        case ScalerBackpanelAppActionTypes.submitTaskRequestActionType:
            // Should do some UI locking and give feedback
            return state;
        case ScalerBackpanelAppActionTypes.submitTaskFailureActionType:
            // Should alert user and unlock UI
            return state;
        case ScalerBackpanelAppActionTypes.submitTaskSuccessActionType:
            return _.cloneDeep(scalerBackpanelAppInitialState);
        case ScalerBackpanelAppActionTypes.getTaskRequestActionType:
            // Should do some UI locking and give feedback
            return state;
        case ScalerBackpanelAppActionTypes.getTaskFailureActionType:
            // Should alert user and unlock UI
            return state;
        case ScalerBackpanelAppActionTypes.getTaskSuccessActionType:
            return updateState(scalerBackpanelAppInitialState, {
                currentTask : action.newTask,
            });
        default:
            return state;
    }
};
