import * as React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {ScalerBackpanelAppActionCreators, ScalerBackpanelAppActionCreatorsClass} from '../actions/actions';
import {ToolEnum} from '../constants/ToolEnum';
import {IScalerBackpanelAppState} from '../models/IScalerBackpanelAppState';

import {AppWithTask} from '../components/AppWithTask';

export interface IScalerBackpanelAppProps {
    scalerBackpanelAppState : IScalerBackpanelAppState;
    dispatch() : void;
};

export class ScalerBackpanelApp extends React.Component<IScalerBackpanelAppProps, {}> {
    private boundActions : ScalerBackpanelAppActionCreatorsClass;

    public componentWillMount() {
        const {
            dispatch,
        } = this.props;
        this.boundActions = bindActionCreators(ScalerBackpanelAppActionCreators, dispatch);
    }

    public render() {
        const {
            boundActions,
        } = this;
        const {
            currentAnnotation,
            currentLabel,
            currentTask,
            selectedTool,
            usingTool,
        } = this.props.scalerBackpanelAppState;
        const handleContainerMouseUpAndMouseOut = boundActions.stopUsingTool;
        return (
            <div
                className={'scaler-backpanel-app-container'}
                onMouseUp={boundActions.stopUsingTool}
                onMouseLeave={boundActions.stopUsingTool}
            >
                { currentTask !== null ?
                    (
                        <AppWithTask
                            currentAnnotation={currentAnnotation}
                            currentLabel={currentLabel}
                            currentTask={currentTask}
                            selectedTool={selectedTool}
                            usingTool={usingTool}
                            clearAnnotations={boundActions.clearAnnotations}
                            updateCurrentAnnotation={boundActions.updateCurrentAnnotation}
                            selectTool={boundActions.selectTool}
                            submitTask={boundActions.submitTask}
                            eraseAnnotation={boundActions.eraseAnnotation}
                            startUsingTool={boundActions.startUsingTool}
                        />
                    )
                    :
                    (
                        <button
                            onClick={boundActions.getTask}
                        >
                            Get a task
                        </button>
                    )
                }
            </div>
        );
    }
}

const mapStateToProps = (state : any) : any => {
    return state;
};

export const ConnectedScalerBackpanelApp = connect<{}, {}, {}>(mapStateToProps)(ScalerBackpanelApp);
