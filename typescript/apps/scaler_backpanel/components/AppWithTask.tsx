import * as React from 'react';

import {ScalerBackpanelAppActionCreators, ScalerBackpanelAppActionCreatorsClass} from '../actions/actions';
import {MINIMUM_ANNOTATION_BOX_SIZE} from '../constants/appConstants';
import {ToolEnum} from '../constants/ToolEnum';
import {IAnnotation} from '../models/IAnnotation';
import {ICurrentTaskState} from '../models/ICurrentTaskState';
import {IScalerBackpanelAppState} from '../models/IScalerBackpanelAppState';

import '../css/AppWithTask.scss';

export interface IAppWithTaskProps {
    currentTask : ICurrentTaskState;
    currentLabel : string | null;
    currentAnnotation : IAnnotation | null;
    selectedTool : ToolEnum;
    usingTool : boolean;
    clearAnnotations() : void;
    eraseAnnotation(index : number) : void;
    submitTask() : void;
    selectTool(tool : ToolEnum, label : string | null) : void;
    updateCurrentAnnotation(top : number, left : number, width : number, height : number) : void;
    startUsingTool() : void;
};

export class AppWithTask extends React.Component<IAppWithTaskProps, {}> {
    private annotationWorkspaceElement : HTMLDivElement | null = null;

    public render() {
        const {
            currentTask,
            currentLabel,
            currentAnnotation,
            selectedTool,
            usingTool,
            clearAnnotations,
            eraseAnnotation,
            submitTask,
            selectTool,
            updateCurrentAnnotation,
            startUsingTool,
        } = this.props;

        const selectEraser = () => selectTool(ToolEnum.ERASER, null);
        const handleMouseMoveInAnnotationWorkspace = selectedTool === ToolEnum.ANNOTATION && usingTool ?
            (event : React.MouseEvent<HTMLDivElement>) => {
                const domNode = this.annotationWorkspaceElement;
                if (domNode !== null && currentAnnotation !== null) {
                    const boundingRectangle = domNode.getBoundingClientRect();
                    const cursorLeft = Math.min(Math.max(event.clientX - boundingRectangle.left, 0), boundingRectangle.width);
                    const cursorTop = Math.min(Math.max(event.clientY - boundingRectangle.top, 0), boundingRectangle.height);
                    let width : number = Math.max(cursorLeft - currentAnnotation.left, MINIMUM_ANNOTATION_BOX_SIZE);
                    width = Math.min(width, boundingRectangle.width - currentAnnotation.left);
                    let height : number = Math.max(cursorTop - currentAnnotation.top, MINIMUM_ANNOTATION_BOX_SIZE);
                    height = Math.min(height, boundingRectangle.height - currentAnnotation.top);
                    updateCurrentAnnotation(currentAnnotation.top, currentAnnotation.left, width, height);
                } else {
                    throw new Error('unexpected');
                }
            }
            :
            undefined;
        const handleMouseDownInAnnotationWorkSpace = selectedTool === ToolEnum.ANNOTATION ?
            (event : React.MouseEvent<HTMLDivElement>) => {
                const domNode = this.annotationWorkspaceElement;
                if (domNode !== null) {
                    const boundingRectangle = domNode.getBoundingClientRect();
                    const cursorLeft = Math.min(Math.max(event.clientX - boundingRectangle.left, 0), boundingRectangle.width);
                    const cursorTop = Math.min(Math.max(event.clientY - boundingRectangle.top, 0), boundingRectangle.height);
                    const width = Math.min(MINIMUM_ANNOTATION_BOX_SIZE, boundingRectangle.width - cursorLeft);
                    const height = Math.min(MINIMUM_ANNOTATION_BOX_SIZE, boundingRectangle.height - cursorTop);
                    startUsingTool();
                    updateCurrentAnnotation(cursorTop, cursorLeft, width, height);
                } else {
                    throw new Error('unexpected');
                }
            }
            :
            (event : React.MouseEvent<HTMLDivElement>) => {
                startUsingTool();
            };

        const annotationWorkspaceElementRef = (element : HTMLDivElement) => {
            this.annotationWorkspaceElement = element;
        };
        const preventDrag = (event : React.DragEvent<any>) => {
            event.preventDefault();
        };
        return (
        <div className={'app-with-task-container'}>
            <div
                className={'task-instructions'}
            >
                Instructions:
                <br />
                <b>{currentTask.instruction.trim().length > 0 ? currentTask.instruction.trim() : 'No instructions provided.'}</b>
            </div>
            <div
                className={'selected-tool'}
            >
                Selected Tool:
                <br />
                <b>{selectedTool === ToolEnum.ERASER ? 'Eraser' : 'Annotation'}</b>
            </div>
            <div
                className={'current-label'}
            >
                Current Label:
                <br />
                <b>{currentLabel === null ? 'N/A' : currentLabel}</b>
            </div>
            <p>Click anywhere on the image or the black area to begin using your current tool, drag where you intend to use the tool, and release the mouse button to stop using the tool.</p>
            <div
                className={'annotation-workspace-container'}
                style={{
                    padding:'20px',
                    backgroundColor:'black',
                    display: 'inline-block',
                    cursor: 'pointer',
                }}
                onMouseMove={handleMouseMoveInAnnotationWorkspace}
                onMouseDown={handleMouseDownInAnnotationWorkSpace}
                onDragStart={preventDrag}
            >
                <div
                    className={'annotation-workspace'}
                    style={{
                        position: 'relative',
                        display: 'inline-block',
                        backgroundColor: 'white',
                    }}
                    ref={annotationWorkspaceElementRef}
                    onDragStart={preventDrag}
                >
                    <img
                        src={currentTask.attachment}
                        onDragStart={preventDrag}
                        style={{
                            userSelect: 'none',
                        }}
                    />
                    {
                        currentTask.annotations.map((annotation, index) => {
                            const handleMouseOver = selectedTool === ToolEnum.ERASER && usingTool ? () => eraseAnnotation(index) : undefined;
                            const handleMouseDown = selectedTool === ToolEnum.ERASER ? () => eraseAnnotation(index) : undefined;
                            return  (
                                <div
                                    style={{
                                        backgroundColor: 'rgba(0,0,0,.3)',
                                        border: '1px solid black',
                                        color: 'white',
                                        height: annotation.height + 'px',
                                        left: annotation.left + 'px',
                                        position: 'absolute',
                                        top: annotation.top + 'px',
                                        width: annotation.width + 'px',
                                        userSelect: 'none',
                                    }}
                                    key={index}
                                    onMouseOver={handleMouseOver}
                                    onMouseDown={handleMouseDown}
                                >
                                    <span
                                        style={{
                                            pointerEvents: 'none',
                                            userSelect: 'none',
                                        }}
                                    >
                                        {annotation.label}
                                    </span>
                                </div>
                            );
                        })
                    }
                    {currentAnnotation !== null &&
                        (
                            <div
                                style={{
                                    backgroundColor: 'rgba(0,255,0,.3)',
                                    border: '1px solid black',
                                    color: 'white',
                                    height: currentAnnotation.height + 'px',
                                    left: currentAnnotation.left + 'px',
                                    position: 'absolute',
                                    top: currentAnnotation.top + 'px',
                                    width: currentAnnotation.width + 'px',
                                    userSelect: 'none',
                                }}
                            >
                                <span
                                    style={{
                                        pointerEvents: 'none',
                                        userSelect: 'none',
                                    }}
                                >
                                    {currentAnnotation.label}
                                </span>
                            </div>
                        )
                    }
                </div>
            </div>
            <div
                className={'objects-to-label-container'}
            >
                <p>Click one of these to use the annotation tool:</p>
                {
                    currentTask.objectsToAnnotate.map((label, key) => {
                        const selectAnnotationTool = () => selectTool(ToolEnum.ANNOTATION, label);
                        return (
                            <button
                                onClick={selectAnnotationTool}
                                key={key}
                            >
                                {label}
                            </button>
                        );
                    })
                }
            </div>
            <button
                onClick={selectEraser}
            >
                Eraser Tool
            </button>
            <button
                onClick={clearAnnotations}
            >
            Clear All
            </button>
            <button
                onClick={submitTask}
            >
                Submit Task
            </button>
        </div>
        );
    }
}
