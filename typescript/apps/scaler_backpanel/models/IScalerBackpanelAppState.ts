import {ToolEnum} from '../constants/ToolEnum';
import {IAnnotation} from './IAnnotation';
import {ICurrentTaskState} from './ICurrentTaskState';

export interface IScalerBackpanelAppState {
    currentTask : null | ICurrentTaskState;
    usingTool : boolean;
    selectedTool : ToolEnum;
    currentAnnotation : null | IAnnotation;
    currentLabel : null | string;
};
