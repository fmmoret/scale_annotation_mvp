import {IAnnotation} from './IAnnotation';

export interface ICurrentTaskState {
    taskId : string;
    attachment : string;
    instruction : string;
    objectsToAnnotate : string[];
    annotations : IAnnotation[];
};
