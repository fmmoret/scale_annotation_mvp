export interface IAnnotation {
    left : number;
    top : number;
    width : number;
    height : number;
    label : string;
}
