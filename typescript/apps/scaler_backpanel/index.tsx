import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, createStore, Store } from 'redux';
import thunkMiddleware from 'redux-thunk';

import {ConnectedScalerBackpanelApp} from './containers/ScalerBackpanelApp';
import {ScalerBackpanelAppReducer} from './reducers/reducers';

const store = createStore(
    combineReducers({
        scalerBackpanelAppState : ScalerBackpanelAppReducer,
    }),
    applyMiddleware(
        thunkMiddleware,
    ),
);

ReactDOM.render(
    (
        <Provider store={store} >
            <ConnectedScalerBackpanelApp />
        </Provider>
    ),
    document.getElementById('scaler-backpanel-app'),
);
