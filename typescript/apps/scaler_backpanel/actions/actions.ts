import * as $ from 'jquery';

import {ToolEnum} from '../constants/ToolEnum';
import {IScalerBackpanelAppState} from '../models/IScalerBackpanelAppState';
import {ScalerBackpanelAppActionTypes} from './actionTypes';

interface IGetStateReturnType {
    scalerBackpanelAppState : IScalerBackpanelAppState;
};

const submitTask = () : Function => {
    return (dispatch : Function, getState : () => IGetStateReturnType) => {
        dispatch({type: ScalerBackpanelAppActionTypes.submitTaskRequestActionType} as ScalerBackpanelAppActionTypes.ISubmitTaskRequest);
        const currentTask = getState().scalerBackpanelAppState.currentTask;
        if (currentTask === null) {
            throw new Error('unexpected');
        }
        $.post('/annotation/submit-task', {task_id: currentTask.taskId, annotations: currentTask.annotations.map((value) => JSON.stringify(value))})
            .then((response) => {
                if (response.message !== undefined) {
                    alert(response.message);
                    dispatch({type: ScalerBackpanelAppActionTypes.submitTaskSuccessActionType} as ScalerBackpanelAppActionTypes.ISubmitTaskSuccess);
                } else {
                    dispatch({type: ScalerBackpanelAppActionTypes.submitTaskFailureActionType} as ScalerBackpanelAppActionTypes.ISubmitTaskFailure);
                    throw new Error('unexpected');
                }
            })
            .fail((response) => {
                if (response.message !== undefined) {
                    alert(response.message);
                }
                dispatch({type: ScalerBackpanelAppActionTypes.submitTaskFailureActionType} as ScalerBackpanelAppActionTypes.ISubmitTaskFailure);
            });
    };
};

const getTask = () : Function => {
    return (dispatch : Function, getState : IScalerBackpanelAppState) => {
        dispatch({type: ScalerBackpanelAppActionTypes.getTaskRequestActionType} as ScalerBackpanelAppActionTypes.IGetTaskRequest);
        $.get('/annotation/next-task')
            .then((response) => {
                if (response.task_id !== undefined && response.instruction !== undefined && response.objects_to_annotate !== undefined && response.attachment !== undefined) {
                    dispatch({
                        type: ScalerBackpanelAppActionTypes.getTaskSuccessActionType,
                        newTask : {
                            taskId: response.task_id,
                            annotations: [],
                            objectsToAnnotate: response.objects_to_annotate,
                            instruction: response.instruction,
                            attachment: response.attachment,
                        }} as ScalerBackpanelAppActionTypes.IGetTaskSuccess);
                } else if (response.message !== undefined) {
                    // No tasks in queue
                    alert(response.message);
                } else {
                    dispatch({type: ScalerBackpanelAppActionTypes.getTaskFailureActionType} as ScalerBackpanelAppActionTypes.IGetTaskFailure);
                    throw new Error('unexpected');
                }
            })
            .fail((response) => {
                if (response.message !== undefined) {
                    alert(response.message);
                }
                dispatch({type: ScalerBackpanelAppActionTypes.getTaskFailureActionType} as ScalerBackpanelAppActionTypes.IGetTaskFailure);
            });
    };
};

// Using class to get benefits of type without writing out members twice
// that happens with interface + plain object implementation
export class ScalerBackpanelAppActionCreatorsClass {
    // async actions
    public readonly submitTask = submitTask;
    public readonly getTask = getTask;

    // actions
    public readonly selectTool = (tool : ToolEnum, label : string | null) : ScalerBackpanelAppActionTypes.ISelectTool => ({type: ScalerBackpanelAppActionTypes.selectToolActionType, tool, label});
    public readonly startUsingTool = () : ScalerBackpanelAppActionTypes.IStartUsingTool => ({type: ScalerBackpanelAppActionTypes.startUsingToolActionType});
    public readonly stopUsingTool = () : ScalerBackpanelAppActionTypes.IStopUsingTool => ({type: ScalerBackpanelAppActionTypes.stopUsingToolActionType});
    public readonly updateCurrentAnnotation = (top : number, left : number, width : number, height : number) : ScalerBackpanelAppActionTypes.IUpdateCurrentAnnotation =>
        ({type: ScalerBackpanelAppActionTypes.updateCurrentAnnotationActionType, top, left, width, height});
    public readonly eraseAnnotation = (index : number) : ScalerBackpanelAppActionTypes.IEraseAnnotation => ({type: ScalerBackpanelAppActionTypes.eraseAnnotationActionType, index});
    public readonly clearAnnotations = () : ScalerBackpanelAppActionTypes.IClearAnnotations => ({type: ScalerBackpanelAppActionTypes.clearAnnotationsActionType});
};
export const ScalerBackpanelAppActionCreators = new ScalerBackpanelAppActionCreatorsClass();
