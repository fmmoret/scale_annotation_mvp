import {Action} from 'redux';

import {ToolEnum} from '../constants/ToolEnum';
import {ICurrentTaskState} from '../models/ICurrentTaskState';

export namespace ScalerBackpanelAppActionTypes {
    // This is a little ugly and verbose, but it's a hoop to jump through
    // for some of the nicer static things typescript provides
    type selectToolActionTypeType = 'SCALER_BACKPANEL/SELECT_TOOL';
    export const selectToolActionType : selectToolActionTypeType = 'SCALER_BACKPANEL/SELECT_TOOL';
    export interface ISelectTool extends Action {
        type : selectToolActionTypeType;
        tool : ToolEnum;
        label : string | null;
    }

    type startUsingToolActionTypeType = 'SCALER_BACKPANEL/START_USING_TOOL';
    export const startUsingToolActionType : startUsingToolActionTypeType = 'SCALER_BACKPANEL/START_USING_TOOL';
    export interface IStartUsingTool extends Action {
        type : startUsingToolActionTypeType;
    }

    type stopUsingToolActionTypeType = 'SCALER_BACKPANEL/STOP_USING_TOOL';
    export const stopUsingToolActionType : stopUsingToolActionTypeType = 'SCALER_BACKPANEL/STOP_USING_TOOL';
    export interface IStopUsingTool extends Action {
        type : stopUsingToolActionTypeType;
    }

    type updateCurrentAnnotationActionTypeType = 'SCALER_BACKPANEL/UPDATE_CURRENT_ANNOTATION';
    export const updateCurrentAnnotationActionType : updateCurrentAnnotationActionTypeType = 'SCALER_BACKPANEL/UPDATE_CURRENT_ANNOTATION';
    export interface IUpdateCurrentAnnotation extends Action {
        type : updateCurrentAnnotationActionTypeType;
        top : number;
        left : number;
        width : number;
        height : number;
    }

    type eraseAnnotationActionTypeType = 'SCALER_BACKPANEL/ERASE_ANNOTATION';
    export const eraseAnnotationActionType : eraseAnnotationActionTypeType = 'SCALER_BACKPANEL/ERASE_ANNOTATION';
    export interface IEraseAnnotation extends Action {
        type : eraseAnnotationActionTypeType;
        index : number;
    }

    type clearAnnotationsActionTypeType = 'SCALER_BACKPANEL/CLEAR_ANNOTATIONS';
    export const clearAnnotationsActionType : clearAnnotationsActionTypeType = 'SCALER_BACKPANEL/CLEAR_ANNOTATIONS';
    export interface IClearAnnotations extends Action {
        type : clearAnnotationsActionTypeType;
    }

    type submitTaskRequestActionTypeType = 'SCALER_BACKPANEL/SUBMIT_TASK_REQUEST';
    export const submitTaskRequestActionType : submitTaskRequestActionTypeType = 'SCALER_BACKPANEL/SUBMIT_TASK_REQUEST';
    export interface ISubmitTaskRequest extends Action {
        type : submitTaskRequestActionTypeType;
    }

    type submitTaskFailureActionTypeType = 'SCALER_BACKPANEL/SUBMIT_TASK_FAILURE';
    export const submitTaskFailureActionType : submitTaskFailureActionTypeType = 'SCALER_BACKPANEL/SUBMIT_TASK_FAILURE';
    export interface ISubmitTaskFailure extends Action {
        type : submitTaskFailureActionTypeType;
    }

    type submitTaskSuccessActionTypeType = 'SCALER_BACKPANEL/SUBMIT_TASK_SUCCESS';
    export const submitTaskSuccessActionType : submitTaskSuccessActionTypeType = 'SCALER_BACKPANEL/SUBMIT_TASK_SUCCESS';
    export interface ISubmitTaskSuccess extends Action {
        type : submitTaskSuccessActionTypeType;
    }

    type getTaskRequestActionTypeType = 'SCALER_BACKPANEL/GET_TASK_REQUEST';
    export const getTaskRequestActionType : getTaskRequestActionTypeType = 'SCALER_BACKPANEL/GET_TASK_REQUEST';
    export interface IGetTaskRequest extends Action {
        type : getTaskRequestActionTypeType;
    }

    type getTaskFailureActionTypeType = 'SCALER_BACKPANEL/GET_TASK_FAILURE';
    export const getTaskFailureActionType : getTaskFailureActionTypeType = 'SCALER_BACKPANEL/GET_TASK_FAILURE';
    export interface IGetTaskFailure extends Action {
        type : getTaskFailureActionTypeType;
    }

    type getTaskSuccessActionTypeType = 'SCALER_BACKPANEL/GET_TASK_SUCCESS';
    export const getTaskSuccessActionType : getTaskSuccessActionTypeType = 'SCALER_BACKPANEL/GET_TASK_SUCCESS';
    export interface IGetTaskSuccess extends Action {
        type : getTaskSuccessActionTypeType;
        newTask : null | ICurrentTaskState;
    }

    // union of all action types -- this let's us
    // get some static guarantees when we are implementing our reducer
    export type actionTypes = ISelectTool | IStartUsingTool | IStopUsingTool |
        IUpdateCurrentAnnotation | IEraseAnnotation | IClearAnnotations |
        ISubmitTaskRequest | ISubmitTaskFailure | ISubmitTaskSuccess |
        IGetTaskRequest | IGetTaskFailure | IGetTaskSuccess;
};
