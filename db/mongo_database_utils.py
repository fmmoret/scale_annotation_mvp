from pymongo import MongoClient
_dbInfo = {'host': 'ds139428.mlab.com', 'port': 39428, 'db': 'heroku_kjq2s8q3', 'auth': {'user': 'heroku_kjq2s8q3', 'pass': 'fo53igl3hpm9u2if94kvc63ipe'}}
def _getMongoDB(info):
    client = MongoClient(info['host'], info['port'])
    db = client[info['db']]
    db.authenticate(info['auth']['user'], info['auth']['pass'])
    return db

# singleton client
_db = _getMongoDB(_dbInfo)
def getMongoDB():
    return _db

# You could
# import os
# DEBUG = os.environ.get('DJANGO_PROJECT_DEBUG', False)
# if DEBUG:
# do some non-production db stuff.
# else:
# do some production db stuff.
