from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

############
# PAGES
############

def home(request):
    context = RequestContext(request)
    return render_to_response('main/home.html', context)
